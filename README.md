# CHATBOT NLP TB 2021 HEG
Source du cas pratique réalisé dans le cadre de mon travail de Bachelor of Science HES⁠-⁠SO en Informatique de gestion (Business Information Technology) Chatbot : analyse des langages de programmation et du traitement automatique des langues naturelles en 2021 à la Haute école de gestion de Genève.

## Cas pratiques
Le cas pratique a été développés sur le framework Botpress (v12.26.5).

**EcommerceBot**
Exportation du chatbot incluant la conception de conversation, les contextes et les intentions.

**Conversation Design**
Flux de conversation d'EcommerceBot.

## License
MIT License

Copyright (c) 2021 Allan Bowman

